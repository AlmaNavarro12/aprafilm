import 'dart:convert';

import 'package:ApraFilm/models/elenco.dart';
import 'package:ApraFilm/models/movies.dart';
import 'package:ApraFilm/models/serie.dart';
import 'package:ApraFilm/views/constants.dart';
import 'package:http/http.dart' as http;

class Api {
  static const _trendingUrl = "https://api.themoviedb.org/3/trending/movie/day?api_key=${Constants.apiKey}&language=${Constants.language}";
  static const _newUrl = "https://api.themoviedb.org/3/movie/upcoming?api_key=${Constants.apiKey}&language=${Constants.language}";
  static const _sugerenceUrl = "https://api.themoviedb.org/3/movie/now_playing?api_key=${Constants.apiKey}&language=${Constants.language}";
  static const _topUrl = "https://api.themoviedb.org/3/movie/top_rated?api_key=${Constants.apiKey}&language=${Constants.language}";
  static const _topSerie = "https://api.themoviedb.org/3/trending/tv/day?api_key=${Constants.apiKey}&language=${Constants.language}";


  Future<List<MovieTrending>> getTrendingMovies() async {
    final response = await http.get(Uri.parse(_trendingUrl));
    if(response.statusCode == 200 ) {
      final decodedData = json.decode(response.body)['results'] as List;
      return decodedData.map((movie) => MovieTrending.fromJson(movie)).toList();
    } else {
      throw Exception('Algo paso');
    }
  }

  Future<List<MovieTrending>> getNewMovies() async {
    final response = await http.get(Uri.parse(_newUrl));
    if(response.statusCode == 200 ) {
      final decodedData = json.decode(response.body)['results'] as List;
      return decodedData.map((movie) => MovieTrending.fromJson(movie)).toList();
    } else {
      throw Exception('Algo paso');
    }
  }

  Future<List<MovieTrending>> getSugerenceMovies() async {
    final response = await http.get(Uri.parse(_sugerenceUrl));
    if(response.statusCode == 200 ) {
      final decodedData = json.decode(response.body)['results'] as List;
      return decodedData.map((movie) => MovieTrending.fromJson(movie)).toList();
    } else {
      throw Exception('Algo paso');
    }
  }

  Future<List<MovieTrending>> getTopMovies() async {
    final response = await http.get(Uri.parse(_topUrl));
    if(response.statusCode == 200 ) {
      final decodedData = json.decode(response.body)['results'] as List;
      return decodedData.map((movie) => MovieTrending.fromJson(movie)).toList();
    } else {
      throw Exception('Algo paso');
    }
  }

  Future<List<Serie>> getTopSerie() async {
    final response = await http.get(Uri.parse(_topSerie));
    if(response.statusCode == 200 ) {
      final decodedData = json.decode(response.body)['results'] as List;
      return decodedData.map((movie) => Serie.fromJson(movie)).toList();
    } else {
      throw Exception('Algo paso');
    }
  }
}