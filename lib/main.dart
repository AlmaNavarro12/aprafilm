import 'package:ApraFilm/views/colors.dart';
import 'package:ApraFilm/views/splash_page.dart';
import 'package:firebase_core/firebase_core.dart';
import 'package:flutter/material.dart';

void main() async {
  try {
  WidgetsFlutterBinding.ensureInitialized();
  await Firebase.initializeApp();
  runApp(const MyApp());
} catch (error) {
  print("Error al inicializar Firebase o ejecutar la aplicación: $error");
}

}

class MyApp extends StatelessWidget {
  const MyApp({super.key});

  @override
  Widget build(BuildContext context) {
    return MaterialApp(
      debugShowCheckedModeBanner: false,
      theme: ThemeData(
        unselectedWidgetColor: AppColors.textoColor
      ),
      home: const SplashPage(), 
    );
  }
}