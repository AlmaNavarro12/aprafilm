class Elenco {

  String name;
  String profilePath;

  Elenco({
    required this.name,
    required this.profilePath
  });

  factory Elenco.fromJson(Map<String, dynamic> json ) {
    return Elenco(
      name: json["name"] ?? "", 
      profilePath: json["profile_path"] ?? ""
      );
  }
}