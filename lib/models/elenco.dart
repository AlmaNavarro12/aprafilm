class Comentario {
  final String usuarioCorreo;
  final String comentario;
  final String fechaHora;
  final String usuarioFoto;

  Comentario({
    required this.usuarioCorreo,
    required this.comentario,
    required this.fechaHora,
    required this.usuarioFoto

  });
}

class Video {
  final String name;
  final String clave;

  Video({
    required this.name,
    required this.clave,

  });

  factory Video.fromJson(Map<String, dynamic> json ) {
    return Video(
      name: json["name"] ?? "",
      clave: json["key"] ?? "",
      );
  }
}

class Favorito {
  final String movie;
  final String poster;
  final String votacion;
  final String lanzamiento;

  Favorito({
    required this.movie,
    required this.poster,
    required this.votacion,
    required this.lanzamiento
  });
}
