class MovieTrending {
  
  int id;
  String title;
  String name;
  String backDropPath;
  String originalTitle;
  String overview;
  String posterPath;
  String releaseDate;
  double voteAverage;

  MovieTrending({
    required this.id,
    required this.title,
    required this.name,
    required this.backDropPath,
    required this.originalTitle,
    required this.overview,
    required this.posterPath,
    required this.releaseDate,
    required this.voteAverage,
  });

  factory MovieTrending.fromJson(Map<String, dynamic> json ) {
    return MovieTrending(
      id: json["id"].toInt() ?? 0, 
      title: json["title"] ?? "", 
      name: json["name"] ?? "", 
      backDropPath: json["backdrop_path"] ?? "", 
      originalTitle: json["original_title"] ?? "", 
      overview: json["overview"] ?? "", 
      posterPath: json["poster_path"] ?? "", 
      releaseDate: json["release_date"] ?? "", 
      voteAverage: json["vote_average"].toDouble() ?? 0.0,
      );
  }
}