class Serie {
  
  String name;
  String overview;
  String posterPath;
  String firstAirDate;
  double voteAverage;

  Serie({
    required this.name,
    required this.overview,
    required this.posterPath,
    required this.firstAirDate,
    required this.voteAverage,
  });

  factory Serie.fromJson(Map<String, dynamic> json ) {
    return Serie(
      name: json["name"] ?? "", 
      overview: json["overview"] ?? "", 
      posterPath: json["poster_path"] ?? "", 
      firstAirDate: json["first_air_date"] ?? "", 
      voteAverage: json["vote_average"].toDouble() ?? 0.0,
      );
  }
}