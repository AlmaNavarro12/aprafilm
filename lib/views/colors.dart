import 'package:flutter/material.dart';

class AppColors {
  static const Color fondoColor = Color(0xFF2C3E50); 
  static const Color tituloColor = Color(0xFF31A1F2); 
  static const Color textoColor = Color(0xFFFFFFFF); 
  static const Color vinculoColor = Color(0xFF2ECC71);  
  static const Color botonColor = Color(0xFF2BC0B7);  
}
