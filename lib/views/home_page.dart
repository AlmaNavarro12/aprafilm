import 'package:ApraFilm/api/api.dart';
import 'package:ApraFilm/models/movies.dart';
import 'package:ApraFilm/models/serie.dart';
import 'package:ApraFilm/views/constants.dart';
import 'package:ApraFilm/views/peliculas.dart';
import 'package:ApraFilm/views/series.dart';
import 'package:ApraFilm/views/series_details.dart';
import 'package:ApraFilm/widgets/carrusel.dart';
import 'package:ApraFilm/widgets/menu.dart';
import 'package:ApraFilm/widgets/titulo.dart';
import 'package:ApraFilm/widgets/top.dart';
import 'package:flutter/material.dart';
import 'colors.dart';

class HomePage extends StatefulWidget {
  const HomePage({Key? key}) : super(key: key);

  @override
  _HomePageState createState() => _HomePageState();
}

class _HomePageState extends State<HomePage> {
  late Future<List<MovieTrending>> trendingMovies;
  late Future<List<MovieTrending>> newMovies;
  late Future<List<MovieTrending>> sugerenceMovie;
  late Future<List<MovieTrending>> topMovie;
  late Future<List<Serie>> topSerie;

  @override
  void initState() {
    super.initState();
    trendingMovies = Api().getTrendingMovies();
    newMovies = Api().getNewMovies();
    sugerenceMovie = Api().getSugerenceMovies();
    topMovie = Api().getTopMovies();
    topSerie = Api().getTopSerie();
  }

  int _currentIndex = 0;
  final PageController _pageController = PageController();

  @override
  void dispose() {
    _pageController.dispose();
    super.dispose();
  }

  void _onMenuItemSelected(int index) {
    setState(() {
      _currentIndex = index;
    });
    _pageController.animateToPage(
      _currentIndex,
      duration: const Duration(milliseconds: 500),
      curve: Curves.ease,
    );
  }

  final List<String> images = [
    'assets/icono.png',
    'assets/image2.jpg',
    'assets/image3.jpg',
  ];

  @override
  Widget build(BuildContext context) {
    return MaterialApp(
        debugShowCheckedModeBanner: false,
        theme: ThemeData(unselectedWidgetColor: AppColors.textoColor),
        home: WillPopScope(
          onWillPop: () async {
            bool confirmExit = await showDialog(
              context: context,
              builder: (BuildContext context) {
                return AlertDialog(
                  title: Text('¿Estás seguro de que deseas salir?'),
                  actions: <Widget>[
                    TextButton(
                      onPressed: () {
                        Navigator.of(context).pop(false); // Cancelar
                      },
                      child: Text('No'),
                    ),
                    TextButton(
                      onPressed: () {
                        Navigator.of(context).pop(true); // Salir
                      },
                      child: Text('Sí'),
                    ),
                  ],
                );
              },
            );
            return confirmExit ??
                false; // Si no se selecciona ninguna opción, se cancela la acción.
          },
          child: Scaffold(
            backgroundColor: AppColors.fondoColor,
            body: Column(
              children: [
                const SizedBox(height: 35),
                Titulo(),
                const SizedBox(height: 10),
                Container(
                  color: AppColors.fondoColor,
                  child: Padding(
                    padding: const EdgeInsets.symmetric(horizontal: 25.0),
                    child: Row(
                      mainAxisAlignment: MainAxisAlignment.start,
                      children: [
                        _buildMenuItem(0, 'Popular'),
                        const SizedBox(width: 15),
                        _buildMenuItem(1, 'Peliculas'),
                        const SizedBox(width: 15),
                        _buildMenuItem(2, 'Series'),
                      ],
                    ),
                  ),
                ),
                Expanded(
                  child: PageView(
                    controller: _pageController,
                    onPageChanged: (index) {
                      setState(() {
                        _currentIndex = index;
                      });
                    },
                    children: [
                      Center(
                          child: SingleChildScrollView(
                              child: Column(children: [
                        const SizedBox(height: 12),
                        SizedBox(
                            child: FutureBuilder(
                                future: trendingMovies,
                                builder: (context, snapshot) {
                                  if (snapshot.hasError) {
                                    return Center(
                                      child: Text(snapshot.error.toString()),
                                    );
                                  } else if (snapshot.hasData) {
                                    return Carrusel(snapshot: snapshot);
                                  } else {
                                    return const Center(
                                        child: CircularProgressIndicator());
                                  }
                                })),
                        const SizedBox(height: 18),
                        Padding(
                            padding:
                                const EdgeInsets.symmetric(horizontal: 25.0),
                            child: Row(
                                mainAxisAlignment:
                                    MainAxisAlignment.spaceBetween,
                                children: const [
                                  Text(
                                    'Últimos estrenos',
                                    style: TextStyle(
                                        fontFamily: 'Poppins',
                                        fontWeight: FontWeight.bold,
                                        fontSize: 16,
                                        color: AppColors.textoColor),
                                  ),
                                  Icon(
                                    Icons.arrow_forward,
                                    size: 24, // Tamaño del icono
                                    color: Colors.blue, // Color del icono
                                  )
                                ])),
                        const SizedBox(height: 10),
                        SizedBox(
                            child: FutureBuilder(
                                future: newMovies,
                                builder: (context, snapshot) {
                                  if (snapshot.hasError) {
                                    return Center(
                                      child: Text(snapshot.error.toString()),
                                    );
                                  } else if (snapshot.hasData) {
                                    return Top(snapshot: snapshot);
                                  } else {
                                    return const Center(
                                        child: CircularProgressIndicator());
                                  }
                                })),
                        const SizedBox(height: 18),
                        Padding(
                            padding:
                                const EdgeInsets.symmetric(horizontal: 25.0),
                            child: Row(
                                mainAxisAlignment:
                                    MainAxisAlignment.spaceBetween,
                                children: const [
                                  Text(
                                    'Sugeriencias para ti',
                                    style: TextStyle(
                                        fontFamily: 'Poppins',
                                        fontWeight: FontWeight.bold,
                                        fontSize: 16,
                                        color: AppColors.textoColor),
                                  ),
                                  Icon(
                                    Icons.arrow_forward,
                                    size: 24, // Tamaño del icono
                                    color: Colors.blue, // Color del icono
                                  )
                                ])),
                        const SizedBox(height: 10),
                        SizedBox(
                            child: FutureBuilder(
                                future: sugerenceMovie,
                                builder: (context, snapshot) {
                                  if (snapshot.hasError) {
                                    return Center(
                                      child: Text(snapshot.error.toString()),
                                    );
                                  } else if (snapshot.hasData) {
                                    return Top(snapshot: snapshot);
                                  } else {
                                    return const Center(
                                        child: CircularProgressIndicator());
                                  }
                                })),
                        const SizedBox(height: 18),
                        Padding(
                            padding:
                                const EdgeInsets.symmetric(horizontal: 25.0),
                            child: Row(
                                mainAxisAlignment:
                                    MainAxisAlignment.spaceBetween,
                                children: const [
                                  Text(
                                    'Películas más vistas',
                                    style: TextStyle(
                                        fontFamily: 'Poppins',
                                        fontWeight: FontWeight.bold,
                                        fontSize: 16,
                                        color: AppColors.textoColor),
                                  ),
                                  Icon(
                                    Icons.arrow_forward,
                                    size: 24, // Tamaño del icono
                                    color: Colors.blue, // Color del icono
                                  )
                                ])),
                        const SizedBox(height: 10),
                        SizedBox(
                            child: FutureBuilder(
                                future: topMovie,
                                builder: (context, snapshot) {
                                  if (snapshot.hasError) {
                                    return Center(
                                      child: Text(snapshot.error.toString()),
                                    );
                                  } else if (snapshot.hasData) {
                                    return Top(snapshot: snapshot);
                                  } else {
                                    return const Center(
                                        child: CircularProgressIndicator());
                                  }
                                })),
                        const SizedBox(height: 18),
                        Padding(
                            padding:
                                const EdgeInsets.symmetric(horizontal: 25.0),
                            child: Row(
                                mainAxisAlignment:
                                    MainAxisAlignment.spaceBetween,
                                children: const [
                                  Text(
                                    'Series más vistas',
                                    style: TextStyle(
                                        fontFamily: 'Poppins',
                                        fontWeight: FontWeight.bold,
                                        fontSize: 16,
                                        color: AppColors.textoColor),
                                  ),
                                  Icon(
                                    Icons.arrow_forward,
                                    size: 24, // Tamaño del icono
                                    color: Colors.blue, // Color del icono
                                  )
                                ])),
                        const SizedBox(height: 10),
                        SizedBox(
                            child: FutureBuilder(
                                future: topSerie,
                                builder: (context, snapshot) {
                                  if (snapshot.hasError) {
                                    return Center(
                                      child: Text(snapshot.error.toString()),
                                    );
                                  } else if (snapshot.hasData) {
                                    return SizedBox(
                                        height: 270,
                                        width: double.infinity,
                                        child: ListView.builder(
                                          scrollDirection: Axis.horizontal,
                                          physics:
                                              const BouncingScrollPhysics(),
                                          itemCount: 10,
                                          itemBuilder: (BuildContext context,
                                              int index) {
                                            return Padding(
                                                padding:
                                                    const EdgeInsets.all(8.0),
                                                child: GestureDetector(
                                                    onTap: () {
                                                      Navigator.push(
                                                        context,
                                                        MaterialPageRoute(
                                                          builder: (context) =>
                                                              DetailsSerieScreen(
                                                            movie: snapshot
                                                                .data![index],
                                                          ),
                                                        ),
                                                      );
                                                    },
                                                    child: SizedBox(
                                                        height: 200,
                                                        width: 150,
                                                        child:
                                                            Column(children: [
                                                          Image.network(
                                                            '${Constants.imagePath}${snapshot.data![index].posterPath}',
                                                            filterQuality:
                                                                FilterQuality
                                                                    .high,
                                                            fit: BoxFit.cover,
                                                          ),
                                                          const SizedBox(
                                                              height: 5),
                                                          Text(
                                                            snapshot
                                                                .data![index]
                                                                .name,
                                                            style: const TextStyle(
                                                                fontFamily:
                                                                    'Poppins',
                                                                fontSize: 11,
                                                                color: AppColors
                                                                    .textoColor),
                                                            maxLines:
                                                                1, // Establecer el número máximo de líneas a 1
                                                            overflow: TextOverflow
                                                                .ellipsis, // Mostrar "..." si el texto se corta
                                                          ),
                                                        ]))));
                                          },
                                        ));
                                  } else {
                                    return const Center(
                                        child: CircularProgressIndicator());
                                  }
                                })),
                        const SizedBox(height: 18),
                      ]))),
                      FutureBuilder(
                          future: newMovies,
                          builder: (context, snapshot) {
                            if (snapshot.hasError) {
                              return Center(
                                child: Text(snapshot.error.toString()),
                              );
                            } else if (snapshot.hasData) {
                              return MovieGrid(snapshot: snapshot);
                            } else {
                              return const Center(
                                  child: CircularProgressIndicator());
                            }
                          }),
                      FutureBuilder(
                          future: topSerie,
                          builder: (context, snapshot) {
                            if (snapshot.hasError) {
                              return Center(
                                child: Text(snapshot.error.toString()),
                              );
                            } else if (snapshot.hasData) {
                              return SerieGrid(snapshot: snapshot);
                            } else {
                              return const Center(
                                  child: CircularProgressIndicator());
                            }
                          }),
                    ],
                  ),
                ),
                Menu()
              ],
            ),
          ),
        ));
  }

  Widget _buildMenuItem(int index, String text) {
    return InkWell(
      onTap: () => _onMenuItemSelected(index),
      child: Column(
        children: [
          Text(
            text,
            style: TextStyle(
              fontFamily: index == _currentIndex ? 'Poppins' : 'Poppins-t',
              fontSize: 18,
              color:
                  index == _currentIndex ? AppColors.textoColor : Colors.grey,
            ),
          ),
          Container(
            height: 2,
            width: 20,
            color: index == _currentIndex
                ? AppColors.vinculoColor
                : Colors.transparent,
          ),
        ],
      ),
    );
  }
}
