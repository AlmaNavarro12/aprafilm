import 'package:ApraFilm/views/home_page.dart';
import 'package:ApraFilm/views/register_page.dart';
import 'package:firebase_auth/firebase_auth.dart';
import 'package:flutter/material.dart';
import 'package:ApraFilm/views/colors.dart';
import 'package:flutter/services.dart';
import 'package:google_sign_in/google_sign_in.dart';

class LoginPage extends StatefulWidget {
  LoginPage({Key? key}) : super(key: key);

  @override
  State<LoginPage> createState() => _LoginPageState();
}

class _LoginPageState extends State<LoginPage> {
  final TextEditingController _correo = TextEditingController();
  TextEditingController _contrasena1 = TextEditingController();

  bool _botonRegistrar = false;
  bool _obscurePassword = true;

  @override
  void initState() {
    super.initState();
    _correo.addListener(_checarBoton);
    _contrasena1.addListener(_checarBoton);
  }

  void _checarBoton() {
    setState(() {
      _botonRegistrar = _correo.text.isNotEmpty &&
          _contrasena1.text.isNotEmpty &&
          _contrasena1.text.length >= 8;
    });
  }

  @override
  void dispose() {
    super.dispose();
    _correo.dispose();
    _contrasena1.dispose();
  }

  Future<void> signInWithEmailAndPassword(String email, String password) async {
    try {
      UserCredential userCredential =
          await FirebaseAuth.instance.signInWithEmailAndPassword(
        email: email,
        password: password,
      );
      User? user = userCredential.user;

      if (user != null) {
        if (user.emailVerified) {
          // El usuario ha verificado su correo electrónico, permitir el inicio de sesión
          Navigator.pushReplacement(
              context, MaterialPageRoute(builder: (context) => HomePage()));
        } else {
          // El usuario no ha verificado su correo electrónico, mostrar un mensaje de error
          showErrorDialog('Error al iniciar sesión',
              'Debes verificar tu dirección de correo electrónico antes de iniciar sesión. \n\nPor favor, revisa tu bandeja de entrada y sigue las instrucciones para verificar tu correo electrónico.');
        }
      }
    } catch (e) {
      print(e);
      if (e is FirebaseAuthException) {
        if (e.code == 'user-not-found') {
          showErrorDialog('Error al iniciar sesión',
              'No se pudo iniciar sesión. Tu cuenta no está registrada. \n\nDa clic en el botón REGISTRATE.');
        } else if (e.code == 'wrong-password') {
          showErrorDialog('Error al iniciar sesión',
              'Su contraseña es incorrecta. Intenta nuevamente.');
        } else if (e.code == 'INVALID_LOGIN_CREDENTIALS') {
          showErrorDialog('Error al iniciar sesión',
              'No se pudo iniciar sesión. Tu correo o contraseña son erroneos.');
        }
      } else {
        showErrorDialog('Error al iniciar sesión',
            'Hubo un problema con la conexión. Intenta nuevamente más tarde.');
      }
    }
  }

  final FirebaseAuth _auth = FirebaseAuth.instance;
final GoogleSignIn googleSignIn = GoogleSignIn();

Future<User?> signInWithGoogle() async {
  try {
    final GoogleSignInAccount? googleSignInAccount = await googleSignIn.signIn();
    if (googleSignInAccount != null) {
      final GoogleSignInAuthentication googleSignInAuthentication = await googleSignInAccount.authentication;
      final AuthCredential credential = GoogleAuthProvider.credential(
        accessToken: googleSignInAuthentication.accessToken,
        idToken: googleSignInAuthentication.idToken,
      );

      final UserCredential authResult = await _auth.signInWithCredential(credential);
      final User? user = authResult.user;

      if (user != null) {
         Navigator.pushReplacement(
              context, MaterialPageRoute(builder: (context) => HomePage()));
        return user;
      } else {
         showErrorDialog('Error al iniciar sesión',
            'Hubo un problema con la conexión. Intenta nuevamente más tarde.');
        return null;
      }
    } else {
       showErrorDialog('Error al iniciar sesión',
            'Se canceló el inicio de sesión con google. Intenta nuevamente más tarde.');
      return null;
    }
  } catch (e) {
     showErrorDialog('Error al iniciar sesión',
            'Hubo un problema con la conexión. Intenta nuevamente más tarde.');
    print("Error al iniciar sesión con Google: $e");
    return null;
  }
}


  void showErrorDialog(String title, String content) {
    showDialog(
      context: context,
      builder: (context) {
        return Stack(
          children: [
            // Fondo semitransparente
            Opacity(
              opacity: 0.6, // Ajusta la opacidad según tu preferencia
              child: ModalBarrier(
                dismissible: false,
                color: Colors.black,
              ),
            ),
            AlertDialog(
              title: Row(
                children: [
                  Icon(
                    Icons.warning_amber,
                    color: Colors.amber,
                  ),
                  SizedBox(width: 5),
                  Text(
                    title,
                    style: TextStyle(
                      color: Colors.amber,
                    ),
                  ),
                ],
              ),
              content: Text(
                content,
                style: TextStyle(
                  color: AppColors.fondoColor,
                  fontFamily: 'Poppins-ExtraLight',
                ),
              ),
            ),
          ],
        );
      },
    );
  }

  @override
  Widget build(BuildContext context) {
    return Scaffold(
        backgroundColor: AppColors.fondoColor,
        body: SingleChildScrollView(
            child: Column(
          children: <Widget>[
            // Logo y mensaje de bienvenida centrados
            const SizedBox(height: 50),
            Center(
              child: Column(
                children: [
                  Image.asset(
                    'assets/icono.png',
                    width: 130,
                    height: 130,
                  ),
                  const Text(
                    'BIENVENIDO',
                    style: TextStyle(
                        fontFamily: 'Poppins',
                        fontWeight: FontWeight.bold,
                        fontSize: 45,
                        color: AppColors
                            .textoColor, // Puedes ajustar el color según tus necesidades.
                        height: 1.0),
                  ),
                  const Text(
                    'APRAFILM',
                    style: TextStyle(
                      fontFamily: 'Poppins',
                      fontWeight: FontWeight.bold,
                      fontSize: 55,
                      color: AppColors.tituloColor,
                      height: 1.0,
                    ),
                  )
                ],
              ),
            ),
            const SizedBox(height: 30),
            Padding(
                padding: const EdgeInsets.symmetric(horizontal: 16.0),
                child: Column(
                    crossAxisAlignment: CrossAxisAlignment.start,
                    children: <Widget>[
                      const Text(
                        'Correo electrónico',
                        style: TextStyle(
                          fontFamily: 'Poppins',
                          fontSize: 16,
                          fontWeight: FontWeight.bold,
                          color: AppColors.vinculoColor,
                        ),
                      ),
                      const SizedBox(height: 5),
                      TextField(
                        controller: _correo,
                        decoration: InputDecoration(
                          contentPadding: EdgeInsets.symmetric(
                              vertical: 10.0, horizontal: 10),
                          border: OutlineInputBorder(),
                          filled: true,
                          fillColor: Colors.white,
                          hintText: 'Introduce tu correo electrónico.',
                          labelStyle: TextStyle(
                            color: Color.fromARGB(255, 82, 82, 82),
                          ),
                          hintStyle: TextStyle(
                            color: Colors.grey,
                          ),
                        ),
                        keyboardType: TextInputType.emailAddress,
                        style: TextStyle(
                          fontSize: 14,
                        ),
                      ),
                      const SizedBox(height: 30),
                      const Text(
                        'Contraseña',
                        style: TextStyle(
                          fontFamily: 'Poppins',
                          fontSize: 16,
                          fontWeight: FontWeight.bold,
                          color: AppColors.vinculoColor,
                        ),
                      ),
                      const SizedBox(height: 2),
                      TextField(
                        controller: _contrasena1,
                        obscureText: _obscurePassword,
                        decoration: InputDecoration(
                          contentPadding: const EdgeInsets.symmetric(
                              vertical: 10.0, horizontal: 10),
                          border: const OutlineInputBorder(),
                          filled: true,
                          fillColor: Colors.white,
                          hintStyle: TextStyle(
                            color: Colors.grey,
                          ),
                          hintText:
                              'Introduce tu contraseña de al menos 8 caracteres.',
                          suffixIcon: IconButton(
                            icon: Icon(_obscurePassword
                                ? Icons.visibility
                                : Icons.visibility_off),
                            color: Colors
                                .grey, // Cambia el color del icono según tus preferencias
                            onPressed: () {
                              setState(() {
                                _obscurePassword = !_obscurePassword;
                              });
                            },
                          ),
                        ),
                        style: const TextStyle(
                          fontSize: 14,
                        ),
                      ),
                    ])),
            const SizedBox(height: 10),
            Container(
                margin: const EdgeInsets.symmetric(horizontal: 16.0),
                width: double.infinity, // Abarca el ancho de la pantalla
                child: Column(children: [
                  GestureDetector(
                      onTap: () {
                        Navigator.push(
                          context,
                          MaterialPageRoute(
                            builder: (context) => HomePage(),
                          ),
                        );
                      },
                      child: const Row(children: [
                        Text(
                          '¿Olvidaste tu contraseña?',
                          style: TextStyle(
                            fontFamily: 'Poppins-ExtraLight',
                            fontSize: 16,
                            color: AppColors.textoColor,
                          ),
                        ),
                        SizedBox(
                          width: 5,
                        ),
                        Text(
                          'Recuperala',
                          style: TextStyle(
                            fontFamily: 'Poppins',
                            fontSize: 15,
                            color: AppColors.vinculoColor,
                          ),
                        ),
                      ])),
                  const SizedBox(height: 40),
                  ElevatedButton(
                    onPressed: _botonRegistrar
                        ? () {
                            String email = _correo.text;
                            String password = _contrasena1.text;
                            signInWithEmailAndPassword(email, password);
                          }
                        : null,
                    style: ElevatedButton.styleFrom(
                      primary: Color(0xFF2BC0B7), // Color de fondo verde
                      minimumSize: Size(double.infinity,
                          45), // Ancho mínimo y altura del botón
                    ),
                    child: const Text(
                      'INICIAR SESIÓN',
                      style: TextStyle(
                          color: Color(0xFFFFFFFF), // Color de texto blanco
                          fontFamily: 'Poppins',
                          fontWeight: FontWeight.bold,
                          fontSize: 16),
                    ),
                  ),
                  const SizedBox(height: 16),
                  ElevatedButton(
                    onPressed: () {
                      signInWithGoogle();
                    },
                    style: ElevatedButton.styleFrom(
                      primary: Color.fromARGB(
                          255, 192, 43, 43), // Color de fondo verde
                      minimumSize: Size(double.infinity,
                          45), // Ancho mínimo y altura del botón
                    ),
                    child: const Text(
                      'INICIAR SESIÓN CON GOOGLE',
                      style: TextStyle(
                          color: Color(0xFFFFFFFF), // Color de texto blanco
                          fontFamily: 'Poppins',
                          fontWeight: FontWeight.bold,
                          fontSize: 16),
                    ),
                  ),
                  const SizedBox(height: 20),
                  Row(mainAxisAlignment: MainAxisAlignment.center, children: [
                    
                    Text(
                      '¿No tienes una cuenta?',
                      style: TextStyle(
                        fontFamily: 'Poppins-ExtraLight',
                        fontSize: 16,
                        color: AppColors.textoColor,
                      ),
                    ),
                    
                  ]),
                  SizedBox(height: 20),
                  ElevatedButton(
                    onPressed: () {
                      Navigator.push(
                        context,
                        MaterialPageRoute(
                          builder: (context) => RegisterPage(),
                        ),
                      );
                      _correo.clear();
                      _contrasena1.clear();
                    },
                    style: ElevatedButton.styleFrom(
                      primary: AppColors.textoColor, // Color de fondo verde
                      minimumSize: Size(double.infinity,
                          45), // Ancho mínimo y altura del botón
                    ),
                    child: const Text(
                      'REGISTRATE',
                      style: TextStyle(
                          color: AppColors.fondoColor, // Color de texto blanco
                          fontFamily: 'Poppins',
                          fontWeight: FontWeight.bold,
                          fontSize: 16),
                    ),
                  ),
                ])),
          ],
        )));
  }
}
