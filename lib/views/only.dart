import 'dart:convert';

import 'package:ApraFilm/models/cast.dart';
import 'package:ApraFilm/models/elenco.dart';
import 'package:ApraFilm/views/constants.dart';
import 'package:flutter_rating_bar/flutter_rating_bar.dart';
import 'package:ApraFilm/models/movies.dart';
import 'package:ApraFilm/views/colors.dart';
import 'package:flutter/material.dart';
import 'package:firebase_database/firebase_database.dart';
import 'package:firebase_auth/firebase_auth.dart';
import 'package:http/http.dart' as http;
import 'package:youtube_player_flutter/youtube_player_flutter.dart';

class DetailsScreen extends StatelessWidget {
  DetailsScreen({super.key, required this.movie});

  final _comentarioController = TextEditingController();

  final MovieTrending movie;

  void writeToDatabase(String userFoto, String userEmail, String movieTitle,
      String commentText) {
    final databaseReference = FirebaseDatabase.instance.reference();

    // Crear un identificador único para cada comentario, por ejemplo, utilizando push().
    final newCommentRef = databaseReference.child('comentarios').push();

    // Obtener la clave única generada para el comentario.
    final commentKey = newCommentRef.key;

    // Crear un mapa con la información del comentario.
    final commentData = {
      'usuario_foto': userFoto,
      'pelicula': movieTitle,
      'usuario_correo': userEmail,
      'comentario': commentText,
      'fecha_hora': DateTime.now()
          .toUtc()
          .toString(), // Puedes usar UTC o la zona horaria de tu elección.
    };

    // Establecer los datos del comentario en Firebase bajo la clave única.
    newCommentRef.set(commentData);
  }

  void writeToFavorite(String userEmail, String movieTitle, String poster,
      double votacion, String lanzamiento) {
    final databaseReference = FirebaseDatabase.instance.reference();

    // Crear un identificador único para cada comentario, por ejemplo, utilizando push().
    final newCommentRef = databaseReference.child('favoritos').push();

    // Obtener la clave única generada para el comentario.
    final commentKey = newCommentRef.key;

    // Crear un mapa con la información del comentario.
    final commentData = {
      'usuario_correo': userEmail,
      'poster': poster,
      'pelicula': movieTitle,
      'votacion': votacion,
      'lanzamiento': lanzamiento,
    };

    // Establecer los datos del comentario en Firebase bajo la clave única.
    newCommentRef.set(commentData);
  }

  @override
  Widget build(BuildContext context) {
    return Scaffold(
      backgroundColor: AppColors.fondoColor,
      body: CustomScrollView(
        slivers: [
          SliverAppBar(
            expandedHeight: 300,
            pinned: true,
            floating: false,
            backgroundColor: AppColors.fondoColor,
            flexibleSpace: FlexibleSpaceBar(
              background: Image.network(
                '${Constants.imagePath}${movie.backDropPath}',
                filterQuality: FilterQuality.high,
                fit: BoxFit.cover,
              ),
            ),
          ),
          SliverToBoxAdapter(
            child: Padding(
              padding: const EdgeInsets.all(12),
              child: Column(
                crossAxisAlignment: CrossAxisAlignment.center,
                children: [
                  Text(
                    'Disponible',
                    style: TextStyle(
                      fontFamily: 'Poppins',
                      fontSize: 14,
                      color: AppColors.textoColor,
                    ),
                  ),
                  Text(
                    movie.title,
                    style: TextStyle(
                      fontFamily: 'Poppins',
                      fontWeight: FontWeight.bold,
                      fontSize: 25,
                      color: AppColors.textoColor,
                    ),
                  ),
                  RatingBar.builder(
                    initialRating: movie.voteAverage / 2,
                    minRating: 1,
                    direction: Axis.horizontal,
                    allowHalfRating: true,
                    itemCount: 5,
                    itemSize: 20,
                    itemBuilder: (context, _) => Icon(
                      Icons.star,
                      color: Color.fromARGB(255, 255, 193, 7),
                    ),
                    onRatingUpdate: (rating) {},
                  ),
                  const SizedBox(height: 20),
                  Row(
                    children: [
                      Expanded(
                        child: ElevatedButton(
                          onPressed: () {
                            User? user = FirebaseAuth.instance.currentUser;
                            String? emailUser = user?.email;
                            String? emailFoto = user?.photoURL;

                            writeToFavorite(
                                emailUser!,
                                movie.title,
                                movie.posterPath,
                                movie.voteAverage,
                                movie.releaseDate);
                          },
                          style: ElevatedButton.styleFrom(
                            primary:
                                AppColors.textoColor, // Color de fondo verde
                            minimumSize: Size(double.infinity,
                                45), // Ancho mínimo y altura del botón
                          ),
                          child: const Text(
                            '+ LISTA DE FAVORITOS',
                            style: TextStyle(
                              color:
                                  AppColors.fondoColor, // Color de texto blanco
                              fontFamily: 'Poppins-ExtraLight',
                              fontWeight: FontWeight.bold,
                              fontSize: 18,
                            ),
                          ),
                        ),
                      ),
                    ],
                  ),
                  const SizedBox(height: 20),
                  Column(
                      crossAxisAlignment: CrossAxisAlignment.start,
                      children: [
                        Text(
                          'Descripción',
                          style: TextStyle(
                            fontFamily: 'Poppins',
                            fontWeight: FontWeight.bold,
                            fontSize: 14,
                            color: AppColors.vinculoColor,
                          ),
                        ),
                        const SizedBox(height: 8),
                        Text(movie.overview,
                            style: TextStyle(
                              fontFamily: 'Poppins-ExtraLight',
                              fontSize: 16,
                              color: AppColors.textoColor,
                            ),
                            textAlign: TextAlign.justify),
                      ]),
                  const SizedBox(height: 20),
                  Wrap(
                    alignment: WrapAlignment
                        .center, // Centra los elementos horizontalmente
                    spacing: 15, // Espacio entre los elementos
                    direction: Axis
                        .horizontal, // Asegura que los elementos estén en una sola fila
                    children: [
                      Container(
                        constraints: BoxConstraints(
                            maxWidth: MediaQuery.of(context).size.width / 2 -
                                20), // Ancho máximo
                        padding: const EdgeInsets.all(10),
                        decoration: BoxDecoration(
                          border: Border.all(color: Colors.grey),
                          borderRadius: BorderRadius.circular(10),
                        ),
                        child: Row(
                          children: [
                            Text(
                              'Lanzamiento',
                              style: TextStyle(
                                fontFamily: 'Poppins',
                                fontWeight: FontWeight.bold,
                                fontSize: 14,
                                color: AppColors.vinculoColor,
                              ),
                            ),
                            const SizedBox(width: 8),
                            Text(
                              movie.releaseDate,
                              style: TextStyle(
                                fontFamily: 'Poppins-ExtraLight',
                                fontSize: 14,
                                color: AppColors.textoColor,
                              ),
                              textAlign: TextAlign.justify,
                            ),
                          ],
                        ),
                      ),
                      Container(
                        constraints: BoxConstraints(
                            maxWidth: MediaQuery.of(context).size.width / 2 -
                                20), // Ancho máximo
                        padding: const EdgeInsets.all(10),
                        decoration: BoxDecoration(
                          border: Border.all(color: Colors.grey),
                          borderRadius: BorderRadius.circular(10),
                        ),
                        child: Row(
                          children: [
                            Text(
                              'Calificación',
                              style: TextStyle(
                                fontFamily: 'Poppins',
                                fontWeight: FontWeight.bold,
                                fontSize: 14,
                                color: AppColors.vinculoColor,
                              ),
                            ),
                            const SizedBox(width: 8),
                            Text(
                              " ${movie.voteAverage} / 10",
                              style: TextStyle(
                                fontFamily: 'Poppins-ExtraLight',
                                fontSize: 14,
                                color: AppColors.textoColor,
                              ),
                              textAlign: TextAlign.justify,
                            ),
                          ],
                        ),
                      ),
                    ],
                  ),
                  const SizedBox(height: 30),
                  Column(
                    crossAxisAlignment: CrossAxisAlignment.start,
                    children: <Widget>[
                      Text(
                        'Créditos',
                        style: TextStyle(
                          fontFamily: 'Poppins',
                          fontWeight: FontWeight.bold,
                          fontSize: 14,
                          color: AppColors.vinculoColor,
                        ),
                      ),
                      const SizedBox(height: 10),
                      ElencoPage(peliculaId: movie.id),
                      SizedBox(height: 20.0),
                      Text(
                        'Videos disponibles',
                        style: TextStyle(
                          fontFamily: 'Poppins',
                          fontWeight: FontWeight.bold,
                          fontSize: 14,
                          color: AppColors.vinculoColor,
                        ),
                      ),
                      const SizedBox(height: 10),
                      VideoPage(peliculaId: movie.id),
                      const SizedBox(height: 35),
                      Text(
                        'Comentarios',
                        style: TextStyle(
                          fontFamily: 'Poppins',
                          fontWeight: FontWeight.bold,
                          fontSize: 14,
                          color: AppColors.vinculoColor,
                        ),
                      ),
                      SizedBox(height: 8.0),
                      ComentariosLeer(peliculaTitle: movie.title),
                      
                      SizedBox(height: 20.0),
                      Row(
                        children: <Widget>[
                          Expanded(
                              child: TextField(
                            controller: _comentarioController,
                            decoration: InputDecoration(
                              contentPadding: const EdgeInsets.symmetric(
                                  vertical: 10.0, horizontal: 12),
                              labelText: 'Escribe un comentario...',
                              labelStyle: TextStyle(
                                color: Colors.grey, // Color del texto del hint
                              ),
                              enabledBorder: OutlineInputBorder(
                                borderSide: BorderSide(
                                  color: Colors
                                      .grey, // Color del borde en estado normal
                                ),
                                borderRadius: BorderRadius.circular(
                                    10), // Borde redondeado
                              ),
                              focusedBorder: OutlineInputBorder(
                                borderSide: BorderSide(
                                  color: Colors
                                      .grey, // Color del borde al enfocar el campo
                                ),
                                borderRadius: BorderRadius.circular(
                                    10), // Borde redondeado
                              ),
                            ),
                            style: TextStyle(
                              fontSize: 14,
                              color: Colors
                                  .grey, // Color del texto dentro del campo
                            ),
                          )),
                          SizedBox(width: 15),
                          Container(
                            decoration: BoxDecoration(
                              border: Border.all(
                                color: Colors.grey, // Color del borde
                                width: 1.0, // Ancho del borde
                              ),
                              borderRadius:
                                  BorderRadius.circular(10), // Radio del borde
                            ),
                            child: IconButton(
                              icon: Icon(Icons.send),
                              color: AppColors.botonColor,
                              onPressed: () {
                                String comentario = _comentarioController.text;
                                User? user = FirebaseAuth.instance.currentUser;
                                String? emailUser = user?.email;
                                String? emailFoto =
                                    user?.photoURL ?? 'sin foto';

                                writeToDatabase(emailFoto, emailUser!,
                                    movie.title, comentario);
                                _comentarioController.clear();
                              },
                            ),
                          )
                        ],
                      ),
                      SizedBox(height: 18.0),
                    ],
                  ),
                ],
              ),
            ),
          ),
        ],
      ),
    );
  }
}

class ComentariosLeer extends StatefulWidget {
  final String peliculaTitle;

  ComentariosLeer({Key? key, required this.peliculaTitle}) : super(key: key);

  @override
  State<ComentariosLeer> createState() => _ComentariosLeerState();
}

class _ComentariosLeerState extends State<ComentariosLeer> {
  final DatabaseReference databaseReference =
      FirebaseDatabase.instance.reference();
  List<Comentario> comentarios = [];

  @override
  void initState() {
    super.initState();
    obtenerComentariosEnTiempoReal(widget.peliculaTitle);
  }

  Future<void> obtenerComentariosEnTiempoReal(String peliculaTitle) async {
    final comentariosRef = databaseReference.child('comentarios');

    comentariosRef.onChildAdded.listen((event) {
      DataSnapshot dataSnapshot = event.snapshot;
      Map<dynamic, dynamic>? comentarioMap =
          dataSnapshot.value as Map<dynamic, dynamic>?;

      if (comentarioMap != null && comentarioMap['pelicula'] == peliculaTitle) {
        Comentario comentario = Comentario(
          usuarioFoto: comentarioMap['usuario_foto'],
          usuarioCorreo: comentarioMap['usuario_correo'],
          comentario: comentarioMap['comentario'],
          fechaHora: comentarioMap['fecha_hora'],
        );
        setState(() {
          comentarios.add(comentario);
        });
      }
    });
  }

  @override
  Widget build(BuildContext context) {
    return comentarios.isEmpty
        ? Center(
            child: Text(
              'No hay comentarios disponibles.',
              style: const TextStyle(
            fontFamily: 'Poppins',
            fontSize: 15,
            color: AppColors.textoColor,
          ),
            ),
          )
        : Column(
            children: comentarios.map((comentario) {
              return Padding(
                  padding: const EdgeInsets.only(bottom: 20.0),
                  child: Column(
                    crossAxisAlignment: CrossAxisAlignment.start,
                    children: [
                      Row(
                        mainAxisAlignment: MainAxisAlignment.spaceBetween,
                        children: [
                          Row(
                            children: [
                              CircleAvatar(
                                radius: 10,
                                backgroundColor: Colors.blue,
                                child: comentario.usuarioFoto != 'sin foto'
                                    ? Image.network(
                                        comentario.usuarioFoto,
                                        fit: BoxFit.cover,
                                      )
                                    : Text(
                                        comentario.usuarioCorreo[0],
                                        style: TextStyle(
                                          fontSize: 9,
                                          color: Colors.white,
                                        ),
                                      ),
                              ),
                              SizedBox(
                                  width:
                                      8), // Espacio entre la foto y el nombre
                              Text(
                                comentario.usuarioCorreo,
                                style: TextStyle(
                                  fontFamily: 'Poppins',
                                  fontWeight: FontWeight.bold,
                                  fontSize: 14,
                                  color: AppColors.textoColor,
                                ),
                              ),
                            ],
                          ),
                          Text(
                            comentario.fechaHora,
                            style: TextStyle(
                              fontFamily: 'Poppins',
                              fontSize: 12,
                              color: AppColors.textoColor,
                            ),
                          ),
                        ],
                      ),
                      SizedBox(height: 10.0),
                      Container(
                        width: double.infinity,
                        decoration: BoxDecoration(
                          border: Border.all(
                            color: AppColors.tituloColor, // Color del borde
                            width: 1.0, // Ancho del borde
                          ),
                          borderRadius:
                              BorderRadius.circular(10), // Borde redondeado
                        ),
                        padding: EdgeInsets.all(
                            10.0), // Espacio interno del comentario
                        child: Text(
                          comentario.comentario,
                          style: TextStyle(
                            fontFamily: 'Poppins',
                            fontSize: 16,
                            color:
                                AppColors.tituloColor, // Color personalizable
                          ),
                        ),
                      )
                    ],
                  ));
            }).toList(),
          );
  }
}

class ElencoPage extends StatefulWidget {
  final int peliculaId;

  ElencoPage({Key? key, required this.peliculaId}) : super(key: key);

  @override
  State<ElencoPage> createState() => _ElencoPageState();
}

class _ElencoPageState extends State<ElencoPage> {
  late Future<List<Elenco>> elenco;

  @override
  void initState() {
    super.initState();
    elenco = getElenco(widget.peliculaId);
  }

  Future<List<Elenco>> getElenco(int peliculaId) async {
    try {
      final _topSerie =
          "https://api.themoviedb.org/3/movie/$peliculaId/credits?api_key=${Constants.apiKey}&language=${Constants.language}";
      final response = await http.get(Uri.parse(_topSerie));

      if (response.statusCode == 200) {
        final decodedData =
            json.decode(response.body)['cast'] as List<dynamic>?;
        debugPrint("Algo que quieras imprimir: $decodedData");

        if (decodedData != null) {
          return decodedData.map((movie) => Elenco.fromJson(movie)).toList();
        } else {
          throw Exception('No se encontró elenco.');
        }
      } else {
        throw Exception('Hubo un problema con la solicitud.');
      }
    } catch (error) {
      throw Exception('Error: $error');
    }
  }

  @override
  Widget build(BuildContext context) {
    return SizedBox(
      child: FutureBuilder(
        future: elenco,
        builder: (context, snapshot) {
          if (snapshot.connectionState == ConnectionState.waiting) {
            return Center(child: CircularProgressIndicator());
          } else if (snapshot.hasError) {
            return Center(child: Text('Error: ${snapshot.error}'));
          } else if (snapshot.hasData) {
            return SizedBox(
              height: 270,
              width: double.infinity,
              child: ListView.builder(
                scrollDirection: Axis.horizontal,
                physics: const BouncingScrollPhysics(),
                itemCount: 12,
                itemBuilder: (BuildContext context, int index) {
                  return Padding(
                    padding: EdgeInsets.only(right: 15.0), // Cambia el valor 16.0 según tus necesidades
                    child: SizedBox(
                      height: 200,
                      width: 150,
                      child: Column(
                        children: [
                          Image.network(
                            '${Constants.imagePath}${snapshot.data![index].profilePath}',
                            filterQuality: FilterQuality.high,
                            fit: BoxFit.cover,
                          ),
                          const SizedBox(height: 5),
                          Text(
                            snapshot.data![index].name,
                            style: const TextStyle(
                              fontFamily: 'Poppins',
                              fontSize: 14,
                              color: AppColors.textoColor,
                            ),
                            maxLines: 1,
                            overflow: TextOverflow.ellipsis,
                          ),
                        ],
                      ),
                    ),
                  );
                },
              ),
            );
          } else {
            return Center(
                child: Text(
              'No se encontraron datos.',
             style: const TextStyle(
            fontFamily: 'Poppins',
            fontSize: 15,
            color: AppColors.textoColor,
          ),
            ));
          }
        },
      ),
    );
  }
}

class VideoPage extends StatefulWidget {
  final int peliculaId;

  VideoPage({Key? key, required this.peliculaId}) : super(key: key);

  @override
  State<VideoPage> createState() => _VideoPageState();
}

class _VideoPageState extends State<VideoPage> {
  late Future<List<Video>> video;
  late YoutubePlayerController _controller;

  @override
  void initState() {
    super.initState();
    video = getVideo(widget.peliculaId);
  }

  Future<List<Video>> getVideo(int peliculaId) async {
    try {
      final _topSerie =
          "https://api.themoviedb.org/3/movie/$peliculaId/videos?api_key=${Constants.apiKey}&language=${Constants.language}";
      final response = await http.get(Uri.parse(_topSerie));

      if (response.statusCode == 200) {
        final decodedData =
            json.decode(response.body)['results'] as List<dynamic>?;
        if (decodedData != null) {
          return decodedData.map((movie) => Video.fromJson(movie)).toList();
        } else {
          throw Exception(peliculaId);
        }
      } else {
        throw Exception(peliculaId);
      }
    } catch (error) {
      throw Exception(peliculaId);
    }
  }

  @override
  Widget build(BuildContext context) {
    return SizedBox(
      child: FutureBuilder(
  future: video,
  builder: (context, snapshot) {
    if (snapshot.connectionState == ConnectionState.waiting) {
      return Center(child: CircularProgressIndicator());
    } else if (snapshot.hasError) {
      return Center(child: Text('Error: ${snapshot.error}'));
    } else if (snapshot.hasData && snapshot.data!.isNotEmpty) {
      final videoId = snapshot.data![0].clave;
      final name = snapshot.data?[0].name ?? 'Nombre no disponible';
      return YoutubePlayerBuilder(
        player: YoutubePlayer(
          controller: YoutubePlayerController(
            initialVideoId: videoId,
            flags: YoutubePlayerFlags(autoPlay: false, mute: false),
          ),
        ),
        builder: (context, player) {
          return Column(
            children: <Widget>[
              player,
              const SizedBox(height: 5),
                          Text(
                            name,
                            style: const TextStyle(
                              fontFamily: 'Poppins',
                              fontSize: 14,
                              color: AppColors.textoColor,
                            ),
                            maxLines: 1,
                            overflow: TextOverflow.ellipsis,
                          ),
            ],
          );
        },
      );
    } else {
      return Center(
        child: Text(
          'No se encontraron videos disponibles',
          style: const TextStyle(
            fontFamily: 'Poppins',
            fontSize: 15,
            color: AppColors.textoColor,
          ),
        )
      );
    }
  },
)

    );
  }
}
