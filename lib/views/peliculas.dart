import 'package:ApraFilm/views/colors.dart';
import 'package:ApraFilm/views/constants.dart';
import 'package:ApraFilm/views/only.dart';
import 'package:flutter/material.dart';

class MovieGrid extends StatelessWidget {
  const MovieGrid({Key? key, required this.snapshot}) : super(key: key);

  final AsyncSnapshot snapshot;

  @override
  Widget build(BuildContext context) {
    return GridView.builder(
      gridDelegate: SliverGridDelegateWithFixedCrossAxisCount(
        crossAxisCount: 3, // Tres columnas en la cuadrícula
        childAspectRatio: 2.0 / 4.0, // Proporción de aspecto de los elementos
        mainAxisSpacing: 10.0, // Espacio vertical entre las celdas
        crossAxisSpacing: 10.0, // Espacio horizontal entre las celdas


      ),
      itemCount: snapshot.data.length,
      itemBuilder: (BuildContext context, int index) {
        return GestureDetector(
          onTap: () {
            Navigator.push(
              context,
              MaterialPageRoute(
                builder: (context) => DetailsScreen(
                  movie: snapshot.data[index],
                ),
              ),
            );
          },
          child: Column(
            crossAxisAlignment: CrossAxisAlignment.start,
            children: [
             ClipRRect(
                        borderRadius: BorderRadius.circular(8),
                        child: SizedBox(
                            width: 150,
                            child: Column(children: [
                              Image.network(
                                  filterQuality: FilterQuality.high,
                                  fit: BoxFit.cover,
                                  '${Constants.imagePath}${snapshot.data![index].posterPath}'),
                              const SizedBox(height: 5),
                              Text(
                                snapshot.data![index].title,
                                style: TextStyle(
                                    fontFamily: 'Poppins',
                                    fontSize: 13,
                                    color: AppColors.textoColor),
                                maxLines:
                                    1, // Establecer el número máximo de líneas a 1
                                overflow: TextOverflow
                                    .ellipsis, // Mostrar "..." si el texto se corta
                              ),
                              const SizedBox(height: 5),
                              Text(
                                '${snapshot.data![index].voteAverage} /10',
                                style: TextStyle(
                                    fontFamily: 'Poppins',
                                    fontSize: 13,
                                    color: AppColors.textoColor),
                                maxLines:
                                    1, // Establecer el número máximo de líneas a 1
                                overflow: TextOverflow
                                    .ellipsis, // Mostrar "..." si el texto se corta
                              ),
                            ])))
            ],
          ),
        );
      },
    );
  }
}
