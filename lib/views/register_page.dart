import 'package:ApraFilm/views/colors.dart';
import 'package:ApraFilm/views/login_page.dart';
import 'package:firebase_auth/firebase_auth.dart';
import 'package:flutter/material.dart';

class RegisterPage extends StatefulWidget {
  const RegisterPage({Key? key}) : super(key: key);

  @override
  State<RegisterPage> createState() => _RegisterPageState();
}

class _RegisterPageState extends State<RegisterPage> {
  final TextEditingController _correo = TextEditingController();
  TextEditingController _contrasena1 = TextEditingController();
  TextEditingController _contrasena2 = TextEditingController();
  bool _ch1 = false;
  bool _botonRegistrar = false;
  bool _obscurePassword = true;
  bool _passwordsMatch = true;

  @override
  void initState() {
    super.initState();
    _correo.addListener(_checarBoton);
    _contrasena1.addListener(_checarBoton);
    _contrasena2.addListener(_checarBoton);
  }

  void _checarBoton() {
    setState(() {
      _botonRegistrar = _correo.text.isNotEmpty &&
          _contrasena1.text.isNotEmpty &&
          _contrasena2.text.isNotEmpty &&
          _contrasena1.text.length >= 8 &&
          _contrasena2.text.length >= 8 &&
          _ch1;
    });
  }

  void _chacarPasswords() {
    final password1 = _contrasena1.text;
    final password2 = _contrasena2.text;

    if (password1 != password2) {
      showDialog(
          context: context,
          builder: (BuildContext context) {
            return Stack(children: [
              // Fondo semitransparente
              Opacity(
                opacity: 0.6, // Ajusta la opacidad según tu preferencia
                child: ModalBarrier(
                  dismissible: false,
                  color: Colors.black,
                ),
              ),
              AlertDialog(
                title: Row(children: [
                  Icon(Icons.warning_amber, color: Colors.amber),
                  SizedBox(width: 5),
                  Text(
                      style: TextStyle(color: Colors.amber),
                      'Error en el formulario.'),
                ]),
                content: Text(
                  'Las contraseñas son diferentes. Revisa e intentalo nuevamente.',
                  style: TextStyle(
                      color: AppColors.fondoColor,
                      fontFamily: 'Poppins-ExtraLight'),
                ),
              )
            ]);
          });
    } else {
      String email = _correo.text;
      String password = _contrasena2.text;

      registerWithEmailAndPassword(email, password);
    }
  }

  @override
  void dispose() {
    super.dispose();
    _correo.dispose();
    _contrasena1.dispose();
    _contrasena2.dispose();
  }

Future<void> registerWithEmailAndPassword(String email, String password) async {
  try {
    UserCredential userCredential = await FirebaseAuth.instance.createUserWithEmailAndPassword(
      email: email,
      password: password,
    );
    User? user = userCredential.user;

    if (user != null) {
      // Enviar correo de verificación
      await user.sendEmailVerification();

      showDialog(
        context: context,
        builder: (context) {
          return Stack(children: [
            // Fondo semitransparente
            Opacity(
              opacity: 0.6, // Ajusta la opacidad según tu preferencia
              child: ModalBarrier(
                dismissible: false,
                color: Colors.black,
              ),
            ),
            AlertDialog(
              title: Row(children: [
                Icon(Icons.check_circle, color: Colors.green),
                SizedBox(width: 5),
                Text(
                  'Registro exitoso',
                  style: TextStyle(color: Colors.green),
                ),
              ]),
              content: Text(
                'Se ha enviado un correo de verificación a tu dirección de correo electrónico. \n\nPor favor, verifica tu correo para completar el registro.',
                style: TextStyle(
                  color: AppColors.fondoColor,
                  fontFamily: 'Poppins-ExtraLight',
                ),
              ),
            )
          ]);
        },
      );
    } else {
      showErrorDialog('Error al registrar la cuenta', 'Intentalo nuevamente.');
    }
   } catch (e) {
    print(e);
    if (e is FirebaseAuthException) {
      if (e.code == 'email-already-in-use') {
        showErrorDialog('Error al registrar la cuenta', 'El correo electrónico que ingresaste ya se encuentra en uso. \n\nIntenta con otro diferente.');
      } 
    } else {
      showErrorDialog('Error al iniciar sesión', 'Hubo un problema al iniciar sesión. Intenta nuevamente más tarde.');
    }
  }
}


void showErrorDialog(String title, String content) {
  showDialog(
    context: context,
    builder: (context) {
      return Stack(
        children: [
          // Fondo semitransparente
          Opacity(
            opacity: 0.6, // Ajusta la opacidad según tu preferencia
            child: ModalBarrier(
              dismissible: false,
              color: Colors.black,
            ),
          ),
          AlertDialog(
            title: Row(
              children: [
                Icon(
                  Icons.warning_amber,
                  color: Colors.amber,
                ),
                SizedBox(width: 5),
                Text(
                  title,
                  style: TextStyle(
                    color: Colors.amber,
                  ),
                ),
              ],
            ),
            content: Text(
              content,
              style: TextStyle(
                color: AppColors.fondoColor,
                fontFamily: 'Poppins-ExtraLight',
              ),
            ),
          ),
        ],
      );
    },
  );
}

  @override
  Widget build(BuildContext context) {
    return Scaffold(
        backgroundColor: AppColors.fondoColor,
        body: CustomScrollView(slivers: [
          const SliverAppBar(
            expandedHeight: 30,
            backgroundColor: AppColors.fondoColor,
            pinned: true,
            floating: false,
          ),
          SliverToBoxAdapter(
              child: Column(
            children: <Widget>[
              Center(
                child: Column(
                  children: [
                    Image.asset(
                      'assets/icono.png',
                      width: 130,
                      height: 130,
                    ),
                    const Text(
                      'CREA UNA',
                      style: TextStyle(
                          fontFamily: 'Poppins',
                          fontWeight: FontWeight.bold,
                          fontSize: 45,
                          color: AppColors
                              .textoColor, // Puedes ajustar el color según tus necesidades.
                          height: 1.0),
                    ),
                    const Text(
                      'CUENTA',
                      style: TextStyle(
                        fontFamily: 'Poppins',
                        fontWeight: FontWeight.bold,
                        fontSize: 55,
                        color: AppColors.tituloColor,
                        height: 1.0,
                      ),
                    )
                  ],
                ),
              ),
              const SizedBox(height: 40),
              Padding(
                  padding: const EdgeInsets.symmetric(horizontal: 16.0),
                  child: Column(
                      crossAxisAlignment: CrossAxisAlignment.start,
                      children: <Widget>[
                        const Text(
                          'Correo electrónico',
                          style: TextStyle(
                            fontFamily: 'Poppins',
                            fontSize: 16,
                            fontWeight: FontWeight.bold,
                            color: AppColors.vinculoColor,
                          ),
                        ),
                        const SizedBox(height: 5),
                        TextField(
                          controller: _correo,
                          decoration: const InputDecoration(
                            contentPadding: EdgeInsets.symmetric(
                                vertical: 10.0, horizontal: 10),
                            border: OutlineInputBorder(),
                            filled: true,
                            fillColor: Colors.white,
                            hintText:
                                'Ingrese una dirección válida ejemplo@email.com',
                            labelStyle: TextStyle(
                              color: Color.fromARGB(255, 82, 82, 82),
                            ),
                            hintStyle: TextStyle(
                              color: Colors.grey,
                            ),
                          ),
                          keyboardType: TextInputType.emailAddress,
                          style: const TextStyle(
                            fontSize: 14,
                          ),
                        ),
                        const SizedBox(height: 20),
                        const Text(
                          'Contraseña',
                          style: TextStyle(
                            fontFamily: 'Poppins',
                            fontSize: 16,
                            fontWeight: FontWeight.bold,
                            color: AppColors.vinculoColor,
                          ),
                        ),
                        const SizedBox(height: 2),
                        TextField(
                          controller: _contrasena1,
                          obscureText: _obscurePassword,
                          decoration: InputDecoration(
                            contentPadding: const EdgeInsets.symmetric(
                                vertical: 10.0, horizontal: 10),
                            border: const OutlineInputBorder(),
                            filled: true,
                            fillColor: Colors.white,
                            hintText: 'Al menos debe contener 8 caracteres',
                            suffixIcon: IconButton(
                              icon: Icon(_obscurePassword
                                  ? Icons.visibility
                                  : Icons.visibility_off),
                              color: Colors
                                  .grey, // Cambia el color del icono según tus preferencias
                              onPressed: () {
                                setState(() {
                                  _obscurePassword = !_obscurePassword;
                                });
                              },
                            ),
                          ),
                          style: const TextStyle(
                            fontSize: 14,
                          ),
                        ),
                        const SizedBox(height: 20),
                        const Text(
                          'Confirma tu contraseña',
                          style: TextStyle(
                            fontFamily: 'Poppins',
                            fontSize: 16,
                            fontWeight: FontWeight.bold,
                            color: AppColors.vinculoColor,
                          ),
                        ),
                        const SizedBox(height: 2),
                        TextField(
                          controller: _contrasena2,
                          obscureText: _obscurePassword,
                          decoration: InputDecoration(
                            contentPadding: const EdgeInsets.symmetric(
                                vertical: 10.0, horizontal: 10),
                            border: const OutlineInputBorder(),
                            filled: true,
                            fillColor: Colors.white,
                            hintText: 'Repite tu contraseña anterior',
                            suffixIcon: IconButton(
                              icon: Icon(_obscurePassword
                                  ? Icons.visibility
                                  : Icons.visibility_off),
                              color: Colors
                                  .grey, // Cambia el color del icono según tus preferencias
                              onPressed: () {
                                setState(() {
                                  _obscurePassword = !_obscurePassword;
                                });
                              },
                            ),
                          ),
                          style: const TextStyle(
                            fontSize: 14,
                          ),
                        ),
                        const SizedBox(height: 20),
                        GestureDetector(
                          onTap: () {
                            _showPrivacyPolicyDialog(context);
                          },
                          child: ClipRRect(
                            borderRadius: BorderRadius.circular(8),
                            child: RichText(
                              text: const TextSpan(
                                children: [
                                  TextSpan(
                                    text:
                                        'Para obtener información sobre cómo Aprafilm procesa sus datos personales para proporcionar este servicio, lea nuestra ',
                                    style: TextStyle(
                                      fontFamily: 'Poppins-ExtraLight',
                                      fontSize: 11,
                                      color: AppColors.textoColor,
                                    ),
                                  ),
                                  TextSpan(
                                    text: 'Políticas de privacidad',
                                    style: TextStyle(
                                      fontFamily: 'Poppins',
                                      fontSize: 11,
                                      color: AppColors.vinculoColor,
                                    ),
                                  ),
                                ],
                              ),
                            ),
                          ),
                        ),
                        const SizedBox(height: 10),
                        Row(
                            mainAxisAlignment: MainAxisAlignment.start,
                            children: [
                              Checkbox(
                                value: _ch1,
                                onChanged: (value) {
                                  setState(() {
                                    _ch1 = value ?? false;
                                    _checarBoton();
                                  });
                                },
                                activeColor: AppColors.botonColor,
                                checkColor: AppColors.textoColor,
                                splashRadius: 20,
                                visualDensity: const VisualDensity(
                                    horizontal: -4, vertical: 0),
                              ),
                              GestureDetector(
                                  onTap: () {
                                    _showPrivacyPolicyDialog(context);
                                  },
                                  child: ClipRRect(
                                      borderRadius: BorderRadius.circular(8),
                                      child: const Row(children: [
                                        Text(
                                          'Acepta estos ',
                                          style: TextStyle(
                                            fontFamily: 'Poppins-ExtraLight',
                                            fontSize: 13,
                                            color: AppColors.textoColor,
                                          ),
                                        ),
                                        Text(
                                          'Términos y condiciones.',
                                          style: TextStyle(
                                            fontFamily: 'Poppins',
                                            fontSize: 13,
                                            color: AppColors.vinculoColor,
                                          ),
                                        ),
                                      ]))),
                            ])
                      ])),
              Container(
                  margin: const EdgeInsets.symmetric(horizontal: 16.0),
                  width: double.infinity, // Abarca el ancho de la pantalla
                  child: Column(children: [
                    const SizedBox(height: 10),
                    ElevatedButton(
                      onPressed: _botonRegistrar
                          ? () {
                              _chacarPasswords();
                            }
                          : null,
                      style: ElevatedButton.styleFrom(
                        primary:
                            const Color(0xFF2BC0B7), // Color de fondo verde
                        minimumSize: const Size(double.infinity,
                            45), // Ancho mínimo y altura del botón
                      ),
                      child: const Text(
                        'REGISTRAR CUENTA',
                        style: TextStyle(
                            color: Color(0xFFFFFFFF), // Color de texto blanco
                            fontFamily: 'Poppins',
                            fontWeight: FontWeight.bold,
                            fontSize: 16),
                      ),
                    ),
                  ])),
            ],
          ))
        ]));
  }
}

void _showPrivacyPolicyDialog(BuildContext context) {
  showDialog(
    context: context,
    builder: (BuildContext context) {
      return AlertDialog(
        title: const Text('AVISO DE PRIVACIDAD APRAFILM'),
        content: SingleChildScrollView(
          child: Column(
            children: [
              // Párrafo 1
              RichText(
                text: TextSpan(
                  children: const <TextSpan>[
                    TextSpan(
                      text:
                          'EL PRESENTE AVISO DE PRIVACIDAD ES APLICABLE A LAS APLICACIONES Y SITIOS DE APRA REPAIRS S.A. DE C.V.\n\n',
                      style: TextStyle(
                        fontWeight: FontWeight.bold,
                        fontFamily: 'Poppins',
                        color: AppColors.fondoColor,
                      ),
                    ),
                    TextSpan(
                      style: TextStyle(
                          color: AppColors.fondoColor, // Color de texto blanco
                          fontFamily: 'Poppins-ExtraLight',
                          fontWeight: FontWeight.bold,
                          fontSize: 14),
                      text:
                          'El presente aviso describe las prácticas de privacidad para los sitios web y aplicaciones móviles de APRA REPAIRS S.A de CV. Esto incluye el sitio web de ApraFilm incluyendo nuestros sitios web móviles y redes sociales. El presente aviso también describe nuestras prácticas en nuestras aplicaciones móviles. Al hacer uso de este sitio y/o plataforma está indicando que está de acuerdo con este aviso, y con el tratamiento de su información de conformidad con los términos del presente Aviso de Privacidad. Los elementos incluidos en este Aviso de Privacidad se hacen de su conocimiento de conformidad con la ley aplicable.\n\n',
                    ),
                  ],
                ),
              ),
              // Párrafo 2
              RichText(
                text: TextSpan(
                  children: const <TextSpan>[
                    TextSpan(
                      text: 'RECABAMOS INFORMACIÓN DE Y ACERCA DE USTED\n\n',
                      style: TextStyle(
                        fontWeight: FontWeight.bold,
                        fontFamily: 'Poppins',
                        color: AppColors.fondoColor,
                      ),
                    ),
                    TextSpan(
                      style: TextStyle(
                          color: AppColors.fondoColor, // Color de texto blanco
                          fontFamily: 'Poppins-ExtraLight',
                          fontWeight: FontWeight.bold,
                          fontSize: 14),
                      text:
                          'Recabamos información de contacto. Podríamos obtener su nombre y su dirección si usa nuestro formulario de contacto en nuestro sitio web. También podríamos obtener su número de teléfono o correo electrónico.\n\n',
                    ),
                  ],
                ),
              ),
              // Párrafo 3, y así sucesivamente...
              RichText(
                text: TextSpan(
                  children: const <TextSpan>[
                    TextSpan(
                      text: 'RECABAMOS INFORMACIÓN DE DISTINTAS MANERAS\n\n',
                      style: TextStyle(
                        fontWeight: FontWeight.bold,
                        fontFamily: 'Poppins',
                        color: AppColors.fondoColor,
                      ),
                    ),
                    TextSpan(
                      style: TextStyle(
                          color: AppColors.fondoColor, // Color de texto blanco
                          fontFamily: 'Poppins-ExtraLight',
                          fontWeight: FontWeight.bold,
                          fontSize: 14),
                      text:
                          'Recabamos información directamente de usted. Por ejemplo, podríamos recabar información si publica una reseña sobre una película o nos contacta. También podríamos recabar información si se registra con nosotros en ciertos sitios, WhatsApp o apps. Recabamos información de usted pasivamente. Podríamos usar herramientas de rastreo como cookies del navegador o web beacons. Recabamos información de los usuarios mientras navegan por nuestro sitio web. Podríamos tener terceros recabando información personal de esta manera. También recabamos información de nuestras aplicaciones móviles y WhatsApp. Recabamos información de usted por medio de terceros. Por ejemplo, las redes sociales podrían otorgarnos información acerca usted. Cuando recabamos información acerca de usted por medio de terceros, consideramos también, de conformidad con la ley local, el aviso de privacidad que le proporcionen dichos terceros. Combinamos información. Por ejemplo, combinamos la información que recabamos en línea con la que recolectamos cuando no estamos en línea. O combinamos la información que nos proporcionaron terceras personas con la que ya teníamos.\n\n',
                    ),
                  ],
                ),
              ),
              RichText(
                text: TextSpan(
                  children: const <TextSpan>[
                    TextSpan(
                      text: 'RECABAMOS INFORMACIÓN DE DISTINTAS MANERAS\n\n',
                      style: TextStyle(
                        fontWeight: FontWeight.bold,
                        fontFamily: 'Poppins',
                        color: AppColors.fondoColor,
                      ),
                    ),
                    TextSpan(
                      style: TextStyle(
                          color: AppColors.fondoColor, // Color de texto blanco
                          fontFamily: 'Poppins-ExtraLight',
                          fontWeight: FontWeight.bold,
                          fontSize: 14),
                      text:
                          'Recabamos información directamente de usted. Por ejemplo, podríamos recabar información si publica una reseña sobre una película o nos contacta. También podríamos recabar información si se registra con nosotros en ciertos sitios, WhatsApp o apps. Recabamos información de usted pasivamente. Podríamos usar herramientas de rastreo como cookies del navegador o web beacons. Recabamos información de los usuarios mientras navegan por nuestro sitio web. Podríamos tener terceros recabando información personal de esta manera. También recabamos información de nuestras aplicaciones móviles y WhatsApp. Recabamos información de usted por medio de terceros. Por ejemplo, las redes sociales podrían otorgarnos información acerca usted. Cuando recabamos información acerca de usted por medio de terceros, consideramos también, de conformidad con la ley local, el aviso de privacidad que le proporcionen dichos terceros. Combinamos información. Por ejemplo, combinamos la información que recabamos en línea con la que recolectamos cuando no estamos en línea. O combinamos la información que nos proporcionaron terceras personas con la que ya teníamos.\n\n',
                    ),
                  ],
                ),
              ),
              RichText(
                text: TextSpan(
                  children: const <TextSpan>[
                    TextSpan(
                      text: 'USAMOS LA INFORMACIÓN CÓMO SE DESCRIBE AQUÍ\n\n',
                      style: TextStyle(
                        fontWeight: FontWeight.bold,
                        fontFamily: 'Poppins',
                        color: AppColors.fondoColor,
                      ),
                    ),
                    TextSpan(
                      style: TextStyle(
                          color: AppColors.fondoColor, // Color de texto blanco
                          fontFamily: 'Poppins-ExtraLight',
                          fontWeight: FontWeight.bold,
                          fontSize: 14),
                      text:
                          'Usamos información para responder sus solicitudes o preguntas. También usamos su información para responderle cuando nos contacte o envíe formularios. \n Podríamos informarle acerca de nuevas características o actualizaciones. También podríamos enviarle notificaciones respecto de nuestras aplicaciones móviles o WhatsApp . Usamos la información para mejorar nuestros servicios. \n Podríamos utilizar su información para personalizar su experiencia con nosotros. Usamos su información para ver la tendencia en los sitios y los intereses del consumidor. Podríamos utilizar su información para realizar mejoras en los sitios. También podríamos combinar información que obtengamos por medio de usted y de terceros. Usamos información para cuestiones de seguridad. Podríamos utilizar información para proteger nuestra compañía y a nuestros clientes. \n También podríamos utilizar información para proteger nuestros sitios web y aplicaciones. Usamos información para comunicarnos con usted. Podríamos contactarle acerca de su retroalimentación. Podríamos contactarle para informarle de cambios en nuestro Aviso de Privacidad o nuestros términos del sitio web. También podríamos enviarle notificaciones instantáneas por medio de nuestras aplicaciones móviles. \n Usamos la información de conformidad con la ley o en caso contrario, se lo notificaremos. Obtendremos su previo consentimiento de conformidad con la ley aplicable.\n\n',
                    ),
                  ],
                ),
              ),
              RichText(
                text: TextSpan(
                  children: const <TextSpan>[
                    TextSpan(
                      text: 'DERECHOS ARCO\n\n',
                      style: TextStyle(
                        fontWeight: FontWeight.bold,
                        fontFamily: 'Poppins',
                        color: AppColors.fondoColor,
                      ),
                    ),
                    TextSpan(
                      style: TextStyle(
                          color: AppColors.fondoColor, // Color de texto blanco
                          fontFamily: 'Poppins-ExtraLight',
                          fontWeight: FontWeight.bold,
                          fontSize: 14),
                      text:
                          'Tiene el derecho de acceder, rectificar, cancelar u objetar (derecho «ARCO») respecto de su información personal. Puede contactarnos por el correo aprarepairs@gmail.com para ejercer cualquiera de los derechos mencionados en la parte superior de conformidad con la legislación mexicana aplicable. .\n\n Le invitamos a ser precavido cuando utilice el internet. Esto incluye no compartir sus contraseñas. Nos quedaremos con su información personal mientras sea necesario o relevante para las prácticas previamente mencionadas en este Aviso de Privacidad. \nTambién nos quedamos con información como lo requiere la ley aplicable. Podríamos tener links de otros sitios o aplicaciones o terceros en nuestras plataformas que no podemos controlar. Si da clic en un hipervínculo de un tercero será llevado a una plataforma que no podemos controlar. Esta información no aplica para las prácticas de ese sitio web o plataforma. \n\nLea los Avisos de Privacidad de otras empresas con cuidado. No nos hacemos responsables por terceros. Siéntase libre de contactarnos si tiene más preguntas acerca de nuestras políticas y prácticas. Puede contactarnos en línea o escribirnos al correo aprarepairs@gmail.com.\nSi tiene alguna pregunta acerca de nuestro Aviso de Privacidad o desea acceder, eliminar, corregir o actualizar su información por favor contáctenos a través de nuestro formulario de contacto. Podríamos actualizar nuestro Aviso de Privacidad. Le informaremos de nuestros cambios de conformidad con la ley. También pondremos la copia actualizada en nuestros sitios web. Por favor revíselo periódicamente para actualizaciones.',
                    ),
                  ],
                ),
              ),
              RichText(
                text: TextSpan(
                  children: const <TextSpan>[
                    TextSpan(
                      text:
                          '\n© 2021 APRA REPAIRS S.A de CV. Todos los derechos reservados.',
                      style: TextStyle(
                        fontWeight: FontWeight.bold,
                        fontFamily: 'Poppins',
                        color: AppColors.fondoColor,
                      ),
                    ),
                  ],
                ),
              ),
            ],
          ),
        ),
        actions: <Widget>[
          TextButton(
            child: const Text('Cerrar'),
            onPressed: () {
              Navigator.of(context).pop();
            },
          ),
        ],
      );
    },
  );
}
