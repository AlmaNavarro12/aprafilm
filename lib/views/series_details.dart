import 'package:ApraFilm/views/constants.dart';
import 'package:flutter_rating_bar/flutter_rating_bar.dart';
import 'package:ApraFilm/models/serie.dart';
import 'package:ApraFilm/views/colors.dart';
import 'package:flutter/material.dart';

class DetailsSerieScreen extends StatelessWidget {
  const DetailsSerieScreen({super.key, required this.movie});

  final Serie movie;

  @override
  Widget build(BuildContext context) {
    return Scaffold(
      backgroundColor: AppColors.fondoColor,
      body: CustomScrollView(
        slivers: [
          SliverAppBar(
            expandedHeight: 300,
            pinned: true,
            floating: false,
            flexibleSpace: FlexibleSpaceBar(
              background: Image.network(
                '${Constants.imagePath}${movie.posterPath}',
                filterQuality: FilterQuality.high,
                fit: BoxFit.cover,
              ),
            ),
          ),
          SliverToBoxAdapter(
            child: Padding(
              padding: const EdgeInsets.all(12),
              child: Column(
                crossAxisAlignment: CrossAxisAlignment.center,
                children: [
                  Text(
                    'Disponible',
                    style: TextStyle(
                      fontFamily: 'Poppins',
                      fontSize: 14,
                      color: AppColors.textoColor,
                    ),
                  ),
                  Text(
                    movie.name,
                    style: TextStyle(
                      fontFamily: 'Poppins',
                      fontWeight: FontWeight.bold,
                      fontSize: 25,
                      color: AppColors.textoColor,
                    ),
                  ),
                  RatingBar.builder(
                    initialRating: movie.voteAverage / 2,
                    minRating: 1,
                    direction: Axis.horizontal,
                    allowHalfRating: true,
                    itemCount: 5,
                    itemSize: 20,
                    itemBuilder: (context, _) => Icon(
                      Icons.star,
                      color: Color.fromARGB(255, 255, 193, 7),
                    ),
                    onRatingUpdate: (rating) {},
                  ),
                  const SizedBox(height: 20),
                   Row(
                    children: [
                      Expanded(
                        child: ElevatedButton(
                          onPressed: () {
                            // Agrega la lógica para la acción "Lista" aquí
                          },
                          style: ElevatedButton.styleFrom(
                            primary:
                                AppColors.textoColor, // Color de fondo verde
                            minimumSize: Size(double.infinity,
                                45), // Ancho mínimo y altura del botón
                          ),
                          child: const Text(
                            '+ LISTA DE FAVORITOS',
                            style: TextStyle(
                              color:
                                  AppColors.fondoColor, // Color de texto blanco
                              fontFamily: 'Poppins-ExtraLight',
                              fontWeight: FontWeight.bold,
                              fontSize: 18,
                            ),
                          ),
                        ),
                      ),
                    ],
                  ),
                  const SizedBox(height: 20),
                  Column(
                      crossAxisAlignment: CrossAxisAlignment.start,
                      children: [
                        Text(
                          'Descripción',
                          style: TextStyle(
                            fontFamily: 'Poppins',
                            fontWeight: FontWeight.bold,
                            fontSize: 14,
                            color: AppColors.vinculoColor,
                          ),
                        ),
                        const SizedBox(height: 8),
                        Text(movie.overview,
                            style: TextStyle(
                              fontFamily: 'Poppins-ExtraLight',
                              fontSize: 16,
                              color: AppColors.textoColor,
                            ),
                            textAlign: TextAlign.justify),
                      ]),
                  const SizedBox(height: 20),
                  Wrap(
  alignment: WrapAlignment.center, // Centra los elementos horizontalmente
  spacing: 15, // Espacio entre los elementos
  direction: Axis.horizontal, // Asegura que los elementos estén en una sola fila
  children: [
    Container(
      constraints: BoxConstraints(maxWidth: MediaQuery.of(context).size.width / 2 - 20), // Ancho máximo
      padding: const EdgeInsets.all(10),
      decoration: BoxDecoration(
        border: Border.all(color: Colors.grey),
        borderRadius: BorderRadius.circular(10),
      ),
      child: Row(
        children: [
          Text(
            'Lanzamiento',
            style: TextStyle(
              fontFamily: 'Poppins',
              fontWeight: FontWeight.bold,
              fontSize: 14,
              color: AppColors.vinculoColor,
            ),
          ),
          const SizedBox(width: 8),
          Text(
            movie.firstAirDate,
            style: TextStyle(
              fontFamily: 'Poppins-ExtraLight',
              fontSize: 14,
              color: AppColors.textoColor,
            ),
            textAlign: TextAlign.justify,
          ),
        ],
      ),
    ),
    Container(
      constraints: BoxConstraints(maxWidth: MediaQuery.of(context).size.width / 2 - 20), // Ancho máximo
      padding: const EdgeInsets.all(10),
      decoration: BoxDecoration(
        border: Border.all(color: Colors.grey),
        borderRadius: BorderRadius.circular(10),
      ),
      child: Row(
        children: [
          Text(
            'Calificación',
            style: TextStyle(
              fontFamily: 'Poppins',
              fontWeight: FontWeight.bold,
              fontSize: 14,
              color: AppColors.vinculoColor,
            ),
          ),
          const SizedBox(width: 8),
          Text(
            " ${movie.voteAverage} / 10",
            style: TextStyle(
              fontFamily: 'Poppins-ExtraLight',
              fontSize: 14,
              color: AppColors.textoColor,
            ),
            textAlign: TextAlign.justify,
          ),
        ],
      ),
    ),
  ],
)


                 
                ],
              ),
            ),
          ),
        ],
      ),
    );
  }
}


