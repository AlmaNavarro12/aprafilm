import 'package:ApraFilm/models/elenco.dart';
import 'package:ApraFilm/views/constants.dart';
import 'package:ApraFilm/views/home_page.dart';
import 'package:firebase_auth/firebase_auth.dart';
import 'package:firebase_database/firebase_database.dart';
import 'package:flutter/material.dart';
import 'package:ApraFilm/views/colors.dart';

class UserPage extends StatefulWidget {
  UserPage({Key? key}) : super(key: key);

  @override
  State<UserPage> createState() => _UserPageState();
}

class _UserPageState extends State<UserPage> {
  User? user = FirebaseAuth.instance.currentUser;

  @override
  void initState() {
    super.initState();
    loadUserData(); // Llama a este método al cargar la pantalla de perfil.
  }

  void loadUserData() {
    // Obtener los datos del usuario actualmente autenticado
    User? user = FirebaseAuth.instance.currentUser;

    if (user != null) {
      // Acceder a los datos del usuario y actualizar la interfaz de usuario
      setState(() {
        String uid = user.uid; // ID único del usuario
        String? email = user.email; // Correo electrónico del usuario
        String displayName =
            user.displayName ?? ''; // Nombre de usuario si está disponible
        String photoURL =
            user.photoURL ?? ''; // URL de la foto de perfil si está disponible
      });
    }
  }

  @override
  Widget build(BuildContext context) {
    return Scaffold(
      backgroundColor: AppColors.fondoColor,
      body: CustomScrollView(
        slivers: [
          SliverAppBar(
            expandedHeight: 30,
            backgroundColor: AppColors.fondoColor,
            pinned: true,
            floating: false,
            leading: IconButton(
              icon: Icon(Icons.arrow_back), // Ícono de flecha de regreso
              color: AppColors.textoColor,
              onPressed: () {
                Navigator.pushReplacement(
                  context,
                  MaterialPageRoute(builder: (context) => HomePage()),
                );
              },
            ),
            flexibleSpace: FlexibleSpaceBar(
              title: Text(
                'Perfil',
                style: TextStyle(
                  fontSize: 25,
                  color: AppColors.textoColor,
                  fontFamily: 'Poppins',
                  fontWeight: FontWeight.bold,
                ),
              ), // Título de la página
            ),
          ),
          SliverToBoxAdapter(
            child: Padding(
              padding: EdgeInsets.symmetric(
                  horizontal: 20), // Añadir márgenes a los lados
              child: Column(
                children: <Widget>[
                  SizedBox(height: 9),
                  Row(
                    children: <Widget>[
                      // Foto de usuario
                      CircleAvatar(
                        radius: 30,
                        backgroundColor:
                            Colors.blue, // Color de fondo del avatar
                        child:
                            user?.photoURL != null && user!.photoURL!.isNotEmpty
                                ? Image.network(
                                    user!.photoURL!,
                                    fit: BoxFit.cover,
                                  )
                                : Text(
                                    user!.email![0],
                                    style: TextStyle(
                                      fontSize: 30,
                                      color: Colors.white, // Color del texto
                                    ),
                                  ),
                      ),

                      SizedBox(width: 10), // Espacio entre la foto y el texto
                      Column(
                        crossAxisAlignment: CrossAxisAlignment.start,
                        children: <Widget>[
                          SizedBox(
                            height: 9,
                          ),
                          Text(
                            user?.displayName ?? user?.email ?? '',
                            style: TextStyle(
                              fontSize: 20,
                              color: AppColors.textoColor,
                              fontFamily: 'Poppins',
                              fontWeight: FontWeight.bold,
                            ),
                          ),
                          Text(
                            '${user?.email ?? ''}',
                            style: TextStyle(
                              fontSize: 16,
                              color: AppColors.textoColor,
                              fontFamily: 'Poppins-ExtraLight',
                            ),
                          ),
                          SizedBox(height: 5),
                          Text(
                            'ID: ${user?.uid ?? ''}',
                            style: TextStyle(
                              fontSize: 10,
                              color: AppColors.textoColor,
                              fontFamily: 'Poppins-ExtraLight',
                            ),
                          ),
                        ],
                      ),
                    ],
                  ),
                  SizedBox(height: 19),
                  Divider(
                    // Línea divisora
                    color: Colors.grey, // Color de la línea divisora
                    height: 20, // Altura de la línea divisora
                    thickness: 1, // Grosor de la línea divisora
                  ),
                  SizedBox(height: 20),
                  Row(
                    children: <Widget>[
                      Icon(
                        Icons.favorite,
                        color: Colors.white,
                        size: 24,
                      ),
                      SizedBox(width: 10), // Espacio entre el icono y el texto
                      Text(
                        'Favoritos',
                        style: TextStyle(
                          fontSize: 25,
                          color: AppColors.textoColor,
                          fontFamily: 'Poppins',
                          fontWeight: FontWeight.bold,
                        ),
                      ),
                    ],
                  ),
                   SizedBox(height: 25 ),
                   FavoriteLeer(usuarioCorreo: '${user?.email ?? ''}')
                ],
              ),
            ),
          ),
        ],
      ),
    );
  }
}

class FavoriteLeer extends StatefulWidget {
  final String usuarioCorreo;

  FavoriteLeer({Key? key, required this.usuarioCorreo}) : super(key: key);

  @override
  State<FavoriteLeer> createState() => _FavoriteLeerState();
}

class _FavoriteLeerState extends State<FavoriteLeer> {
  final DatabaseReference databaseReference =
      FirebaseDatabase.instance.reference();
  List<Favorito> favoritos = [];

  @override
  void initState() {
    super.initState();
    obtenerFavoritosEnTiempoReal(widget.usuarioCorreo);
  }

  Future<void> obtenerFavoritosEnTiempoReal(String usuarioCorreo) async {
    final favoritosRef = databaseReference.child('favoritos');

    favoritosRef.onChildAdded.listen((event) {
      DataSnapshot dataSnapshot = event.snapshot;
      Map<dynamic, dynamic>? favoritosMap =
          dataSnapshot.value as Map<dynamic, dynamic>?;

      if (favoritosMap != null && favoritosMap['usuario_correo'] == usuarioCorreo) {
        Favorito favorito = Favorito(
          poster: favoritosMap['poster'],
          movie: favoritosMap['pelicula'],
          votacion: favoritosMap['votacion'],
          lanzamiento: favoritosMap['lanzamiento'],
        );
        setState(() {
          favoritos.add(favorito);
          print(favoritos);
        });
      }
    });
  }

  @override
  Widget build(BuildContext context) {
    return favoritos.isEmpty
        ? Center(
            child: Text(
              'No hay favoritos agregados.',
              style: const TextStyle(
                fontFamily: 'Poppins',
                fontSize: 15,
                color: AppColors.textoColor,
              ),
            ),
          )
        : Column(
            children: favoritos.map((favorito) {
              return Padding(
                padding: const EdgeInsets.only(bottom: 20.0),
                child: Row(
                  crossAxisAlignment: CrossAxisAlignment.start,
                  children: [
                    // Parte izquierda: Imagen del póster
                    Container(
                      width: 100,
                      height: 150, // Ajusta la altura según tus necesidades
                      child: Image.network(
                        '${Constants.imagePath}${favorito.poster}',
                        filterQuality:
                            FilterQuality.high, // Propiedad filterQuality
                        fit: BoxFit.cover, // Propiedad fit
                      ),
                    ),
                    SizedBox(
                        width:
                            8), // Espacio entre la imagen del póster y el contenido

                    // Parte central: Nombre de la película y detalles
                    Expanded(
                      child: Column(
                        crossAxisAlignment: CrossAxisAlignment.start,
                        children: [
                          Text(
                            favorito.movie,
                            style: TextStyle(
                              fontFamily: 'Poppins',
                              fontWeight: FontWeight.bold,
                              fontSize: 14,
                              color: AppColors.textoColor,
                            ),
                          ),
                          Wrap(
                            alignment: WrapAlignment.center,
                            spacing: 15,
                            direction: Axis.horizontal,
                            children: [
                              Container(
                                constraints: BoxConstraints(
                                  maxWidth:
                                      MediaQuery.of(context).size.width / 2 -
                                          20,
                                ),
                                padding: const EdgeInsets.all(10),
                                decoration: BoxDecoration(
                                  border: Border.all(color: Colors.grey),
                                  borderRadius: BorderRadius.circular(10),
                                ),
                                child: Row(
                                  children: [
                                    Text(
                                      'Lanzamiento',
                                      style: TextStyle(
                                        fontFamily: 'Poppins',
                                        fontWeight: FontWeight.bold,
                                        fontSize: 14,
                                        color: AppColors.vinculoColor,
                                      ),
                                    ),
                                    const SizedBox(width: 8),
                                    Text(
                                      favorito.lanzamiento,
                                      style: TextStyle(
                                        fontFamily: 'Poppins-ExtraLight',
                                        fontSize: 14,
                                        color: AppColors.textoColor,
                                      ),
                                      textAlign: TextAlign.justify,
                                    ),
                                  ],
                                ),
                              ),
                              Container(
                                constraints: BoxConstraints(
                                  maxWidth:
                                      MediaQuery.of(context).size.width / 2 -
                                          20,
                                ),
                                padding: const EdgeInsets.all(10),
                                decoration: BoxDecoration(
                                  border: Border.all(color: Colors.grey),
                                  borderRadius: BorderRadius.circular(10),
                                ),
                                child: Row(
                                  children: [
                                    Text(
                                      'Calificación',
                                      style: TextStyle(
                                        fontFamily: 'Poppins',
                                        fontWeight: FontWeight.bold,
                                        fontSize: 14,
                                        color: AppColors.vinculoColor,
                                      ),
                                    ),
                                    const SizedBox(width: 8),
                                    Text(
                                      "${favorito.votacion} / 10",
                                      style: TextStyle(
                                        fontFamily: 'Poppins-ExtraLight',
                                        fontSize: 14,
                                        color: AppColors.textoColor,
                                      ),
                                      textAlign: TextAlign.justify,
                                    ),
                                  ],
                                ),
                              ),
                            ],
                          )
                        ],
                      ),
                    ),
                  ],
                ),
              );
            }).toList(),
          );
  }
}
