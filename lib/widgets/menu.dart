import 'package:ApraFilm/views/colors.dart';
import 'package:ApraFilm/views/home_page.dart';
import 'package:ApraFilm/views/login_page.dart';
import 'package:ApraFilm/views/user.dart';
import 'package:firebase_auth/firebase_auth.dart';
import 'package:flutter/material.dart';

class Menu extends StatefulWidget {
  Menu({Key? key}) : super(key: key);

  @override
  State<Menu> createState() => _MenuState();
}

class _MenuState extends State<Menu> {
  @override
  void initState() {
    super.initState();
  }

  @override
  Widget build(BuildContext context) {
    return Container(
      color: AppColors.fondoColor,
      child: Row(
        mainAxisAlignment: MainAxisAlignment.spaceAround,
        children: [
          IconButton(
            icon: Icon(Icons.home),
            color: AppColors.botonColor,
            onPressed: () {
              Navigator.pushReplacement(
                        context,
                        MaterialPageRoute(builder: (context) => HomePage()),
                      );
            },
          ),
          IconButton(
            icon: Icon(Icons.search),
            color: AppColors.botonColor,
            onPressed: () {},
          ),
          IconButton(
            icon: Icon(Icons.person),
            color: AppColors.botonColor,
            onPressed: () {
               Navigator.pushReplacement(
                        context, 
                        MaterialPageRoute(builder: (context) => UserPage()),
                      );
            },
          ),
          IconButton(
      icon: Icon(Icons.exit_to_app),
      color: AppColors.botonColor,
      onPressed: () {
        showDialog(
          context: context,
          builder: (BuildContext context) {
            return AlertDialog(
              title: Text("¿Estás seguro de salir?"),
              content: Text("Si sales, se cerrará tu sesión."),
              actions: <Widget>[
                TextButton( // Reemplaza FlatButton por TextButton
                  child: Text("Cancelar"),
                  onPressed: () {
                    Navigator.of(context).pop();
                  },
                ),
                TextButton( // Reemplaza FlatButton por TextButton
                  child: Text("Salir"),
                  onPressed: () {
                    FirebaseAuth.instance.signOut();
                      Navigator.pop(context); // Cierra el diálogo
                      Navigator.pushReplacement(
                        context,
                        MaterialPageRoute(builder: (context) => LoginPage()),
                      );
                  },
                ),
              ],
            );
          },
        );
      },
    ),
        ],
      ),
    );
  }
}
