import 'package:ApraFilm/views/colors.dart';

import 'package:ApraFilm/views/login_page.dart';
import 'package:firebase_auth/firebase_auth.dart';
import 'package:flutter/material.dart';

class Titulo extends StatefulWidget {
  Titulo({Key? key}) : super(key: key);

  @override
  State<Titulo> createState() => _TituloState();
}

class _TituloState extends State<Titulo> {
  @override
  Widget build(BuildContext context) {
    return Padding(
      padding: const EdgeInsets.symmetric(horizontal: 15.0),
      child: Row(
  mainAxisAlignment: MainAxisAlignment.spaceBetween,
  children: <Widget>[
    Row(
      children: <Widget>[
        Image.asset(
          'assets/icono.png',
          width: 50,
          height: 50,
        ),
        const SizedBox(width: 5),
        const Text(
          'APRAFILM',
          style: TextStyle(
            fontFamily: 'Poppins',
            fontWeight: FontWeight.bold,
            fontSize: 45,
            color: AppColors.tituloColor,
          ),
        ),
      ],
    ),
  ],
));
  }
  }