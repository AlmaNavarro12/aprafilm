import 'package:ApraFilm/views/colors.dart';
import 'package:ApraFilm/views/constants.dart';
import 'package:ApraFilm/views/only.dart';
import 'package:flutter/material.dart';

class Top extends StatelessWidget {
  const Top({super.key, required this.snapshot});

  final AsyncSnapshot snapshot;

  @override
  Widget build(BuildContext context) {
    return SizedBox(
        height: 270,
        width: double.infinity,
        child: ListView.builder(
          scrollDirection: Axis.horizontal,
          physics: const BouncingScrollPhysics(),
          itemCount: 10,
          itemBuilder: (BuildContext context, int index) {
            return Padding(
                padding: const EdgeInsets.all(8.0),
                child: GestureDetector(
                    onTap: () {
                      Navigator.push(
                        context,
                        MaterialPageRoute(
                          builder: (context) => DetailsScreen(
                            movie: snapshot.data[index],
                          ),
                        ),
                      );
                    },
                    child: ClipRRect(
                        borderRadius: BorderRadius.circular(8),
                        child: SizedBox(
                            height: 200,
                            width: 150,
                            child: Column(children: [
                              Image.network(
                                  filterQuality: FilterQuality.high,
                                  fit: BoxFit.cover,
                                  '${Constants.imagePath}${snapshot.data![index].posterPath}'),
                              const SizedBox(height: 5),
                              Text(
                                snapshot.data![index].title,
                                style: TextStyle(
                                    fontFamily: 'Poppins',
                                    fontSize: 13,
                                    color: AppColors.textoColor),
                                maxLines:
                                    1, // Establecer el número máximo de líneas a 1
                                overflow: TextOverflow
                                    .ellipsis, // Mostrar "..." si el texto se corta
                              ),
                            ])))));
          },
        ));
  }
}
